//#include <iostm8s207cb.h>
//#include <intrinsics.h> // thu vien ngat
#include "stm8s.h"
//#include "stm8s_it.h"
#include "LCD.h"
#include "Delay.h"
#include "Led7seg.h"
//#include "i2c_soft.h"
//#include "DS1307.h"


volatile int test_var = 0;
volatile char d4,d3,d2,d1;

volatile uint32_t count =0;
volatile uint32_t dl_ms =0;
volatile uint32_t i =0;
unsigned long count_time = 0;
//volatile uint32_t time_keeper=0;
volatile uint8_t Set_up =0;
volatile uint8_t Biendu =0;

//volatile uint32_t time_keeper=0;
volatile int aState;
volatile int aLastState;

void GPIO_Config(void);

void CLK_Config()
{
  CLK_DeInit();
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // f_Master = HSI/1 = 16 Mhz
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1); // f_CPU =f_Master = HSI/1 = 16 Mhz
  while(CLK_GetFlagStatus(CLK_FLAG_HSIRDY) != SET); // wait until HSI ready
}

void GPIO_Config(void)
{
   GPIO_Init(GPIOD,GPIO_PIN_0,GPIO_MODE_IN_PU_NO_IT);
 // GPIO_Init(GPIOD,GPIO_PIN_0,GPIO_MODE_IN_PU_IT);
  GPIO_Init(GPIOE,GPIO_PIN_2,GPIO_MODE_IN_PU_IT);
//  EXTI_DeInit();
  //EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE,EXTI_SENSITIVITY_RISE_ONLY);
 // EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD,EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE,EXTI_SENSITIVITY_FALL_ONLY);
  enableInterrupts();
  }
/*
void I2C_Config()
{
  // SCL_pin
	PB_DDR_DDR4 = 1;
	PB_CR1_C14 = 1;
	PB_ODR_ODR4 = 0;
  // SDA_Pin
	PB_DDR_DDR5 = 1;
	PB_CR1_C15 = 1;
	PB_ODR_ODR5 = 0;	
}
*/

void HienThi_ThoiGian(void)
{
	unsigned char gio = 0,phut = 0,giay = 0;
	unsigned char thu = 0, ngay = 0, thang = 0, nam = 0;

	unsigned char giay_old = 0;
	unsigned char dots_enable = 0;
	unsigned int number_time = 0; //bien tam
	unsigned int number_date = 0; //bien luu thoi gian hien thi len led 7 seg
	unsigned long save_t = 1000;
	unsigned int run_count = 0;
	unsigned char m = 0;

	//reset display : 
	/* truoc khi vao mot chuong trinh gi day can phai reset
	nhat la doi voi hien thi gia tri
	*/
	//Led7Seg_SetData(0000);
	save_time(12,30,00);
	save_date(2,20,7,20);
	
	while(1)
	{
		/*********************************** read time **************************/
		read_time(&gio,&phut,&giay);
		Hienthiso(giay);
	

		number_time = gio * 100 + phut;
		
		/*********************************** show time **************************/
		count_time ++;
		if(giay_old != giay)
		{
			if(++run_count == 4)
			{
				m ^= 1; //dao trang thai
				read_date(&thu,&ngay,&thang,&nam);
				number_date = ngay * 100 + thang; //ngay 20 , thang 7 : 20*100 + 7 = 2007
				run_count = 0;
			}

			save_t = count_time;
			count_time = 0;
			giay_old = giay;
		}
                  
		Delay_ms(1); //moi tac vu delay 1ms

		//break;


	}
}

void EC11_Config()
{
   aLastState = GPIO_ReadInputPin(GPIOA,GPIO_PIN_6);
  while(1)
  {
     aState = GPIO_ReadInputPin(GPIOA,GPIO_PIN_6);
     if ( aState!= aLastState)
     {
       if(GPIO_ReadInputPin(GPIOB,GPIO_PIN_7) != aLastState)
       {
         count --;
       }
       if(GPIO_ReadInputPin(GPIOB,GPIO_PIN_7) != aState)
       {
         count ++;
        // Biendu = 1;
       }   
       d4 = count%10 + '0';
       d3 = (count/10)%10 + '0';
     //  d2 = (count/100)%10 + '0';
     //  d1 = (count/1000) + '0';
       Lcd_Set_Cursor(3,6);
     //  Lcd_Print_Char(d1);
     //  Lcd_Print_Char(d2);
       Lcd_Print_Char(d3);
       Lcd_Print_Char(d4);
       Biendu = 1;
     }
     aLastState = aState;
    
     if(GPIO_ReadInputPin(GPIOD,GPIO_PIN_0)==0&& Biendu != 0)
       {
         Set_up =1 ;
         Biendu=0;
         break;
       }
    }
}


void main()
{
CLK_Config();
//GPIO_Config();
delay_using_timer4_init();
//I2C_Config();
Lcd_Begin();
Led7segConfig();
GPIO_Config();
//EC11_Config();
Lcd_Clear();
Lcd_Set_Cursor(1,1);
Lcd_Print_String("STM8S207CB LCD");
Lcd_Clear();
Lcd_Set_Cursor(1,1);
Lcd_Print_String("PHENIKAA");
Lcd_Set_Cursor(2,1);
Lcd_Print_String("SETUP : ");
Lcd_Set_Cursor(3,1);
Lcd_Print_String("TG : ");
EC11_Config();
//dl_ms = 1000*count; 
//display(1,4);
 //HienThi_ThoiGian();
  while(1)
  {
    if ((GPIO_ReadInputPin(GPIOD,GPIO_PIN_0)==0) &&  (Set_up==0))
        {
         EC11_Config();
        }
     if ((GPIO_ReadInputPin(GPIOD,GPIO_PIN_0)==0) &&  (Set_up==1)) 
         {
          // Delay_ms(1000);
         //  Set_up = 0;
           dl_ms = 1000*count; 
           while(1)
             {
           //    if ((GPIO_ReadInputPin(GPIOD,GPIO_PIN_0)==0) &&  (Set_up==0) )
            //  {
             //   Set_up = 0;
            //    break;
            //   }
               if(Set_up==0 )
                {
                  break;
                }
            
             GPIO_WriteHigh(GPIOD,GPIO_PIN_7);
             Hienthiso(i);
 
             Delay_ms(dl_ms);
             GPIO_WriteLow(GPIOD,GPIO_PIN_7);
      
              Delay_ms(dl_ms);
                i++;
           
           }
           }

  }
}

void assert_failed(u8* file, u32 line) 
{ 
  while (1)
  {
  }
} 
#ifndef __DS1307__H
#define __DS1307__H

#include "i2c_soft.h"

#define ds1307_slavew 0xd0
#define R 1
#define W 0

void ds1307_wadr(unsigned char adr);
void ds1307_wblock(unsigned char adr, unsigned char data);
unsigned char ds1307_rblock(unsigned char addr, unsigned char data[], unsigned char len);
unsigned char BCD2Dec(unsigned char BCD);
unsigned char Dec2BCD(unsigned char Dec);
void get_time(unsigned char *giay, unsigned char*phut,unsigned char*gio);
void get_date(unsigned char *day, unsigned char *date,unsigned char *month , unsigned char *year);
void write_time(unsigned char giay, unsigned char phut, unsigned char gio);
void write_date(unsigned char day,unsigned char date, unsigned char month, unsigned char year);
unsigned int get_temp();


#endif
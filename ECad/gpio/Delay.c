#include "Delay.h"

volatile uint32_t time_keeper=0;


void delay_using_timer4_init(void)
{
 TIM4_TimeBaseInit(TIM4_PRESCALER_128,124);
 TIM4_ClearFlag(TIM4_FLAG_UPDATE);
 TIM4_ITConfig(TIM4_IT_UPDATE,ENABLE);
 enableInterrupts();
 TIM4_Cmd(ENABLE);
}

void Delay_isr(void)
{
	if(TIM4_GetITStatus(TIM4_IT_UPDATE)==SET)
    {
      if(time_keeper!=0)
        {
          time_keeper--;
          }
      TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
      }
}

void Delay_ms(uint32_t time)
{
	time_keeper=time;

	/* Reset Counter Register value */
	TIM4->CNTR = (uint8_t)(0);

	/* Enable Timer */
	TIM4->CR1 |= TIM4_CR1_CEN;

	while(time_keeper);
}
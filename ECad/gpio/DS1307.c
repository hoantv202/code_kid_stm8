#include "DS1307.h"

void ds1307_wadr(unsigned char adr)
{
    start_i2c(ds1307_slavew);
    write_byte_i2c(adr);
    stop_i2c();
}

void ds1307_wblock(unsigned char adr, unsigned char data)
{
    start_i2c(0xd0);
    write_byte_i2c(adr);

    write_byte_i2c(data);
    stop_i2c();
}

unsigned char ds1307_rblock(unsigned char addr, unsigned char data[], unsigned char len)
{
    ds1307_wadr(0x00);
    start_i2c(0xd0);
    write_byte_i2c(addr);
    start_i2c(0xd1);
    for(unsigned char i=0;i<len-1;i++)
    {
        data[i] = read_byte_i2c(0);

    }
    data[len-1] = read_byte_i2c(1);
    stop_i2c();
    return data[0];
}

unsigned char BCD2Dec(unsigned char BCD)
{
    unsigned L,H;
    L = BCD&0XFF;
    H = (BCD>>4)*10;
    return (L+H);
}
unsigned char Dec2BCD(unsigned char Dec)
{
    unsigned L,H;
    L = Dec %10;
    H = (Dec/10)<<4;
    return (L+H);
}

void get_time(unsigned char *giay, unsigned char*phut,unsigned char*gio)
{
    unsigned char tdata[18];
    *giay = BCD2Dec(ds1307_rblock(0x00,tdata,7) & 0x7f);
    *phut = BCD2Dec(ds1307_rblock(0x01,tdata,7) & 0x7f);
    *gio  = BCD2Dec(ds1307_rblock(0x02,tdata,7) & 0x3f);
}

void get_date(unsigned char *day, unsigned char *date,unsigned char *month , unsigned char *year)
{
    unsigned char tdata[18];
    *day =   BCD2Dec(ds1307_rblock(0x03,tdata,7) & 0x0f);
    *date =  BCD2Dec(ds1307_rblock(0x04,tdata,7) & 0x7f);
    *month = BCD2Dec(ds1307_rblock(0x05,tdata,7) & 0x0f);
    *year =  BCD2Dec(ds1307_rblock(0x06,tdata,7) & 0x0f);
}

void write_time(unsigned char giay, unsigned char phut, unsigned char gio)
{
    ds1307_wblock(0x00,Dec2BCD(giay));
    ds1307_wblock(0x01,Dec2BCD(phut));
    ds1307_wblock(0x02,Dec2BCD(gio));
}

void write_date(unsigned char day,unsigned char date, unsigned char month, unsigned char year)
{
    ds1307_wblock(0x03,Dec2BCD(day));
    ds1307_wblock(0x04,Dec2BCD(date));
    ds1307_wblock(0x05,Dec2BCD(month));
    ds1307_wblock(0x06,Dec2BCD(year));
}

unsigned int get_temp()
{
    unsigned char tdata[18];
    unsigned char temp, templ;
    ds1307_wblock(0x11,0x20);
    temp = ds1307_rblock(0x11,tdata,7);
    templ = (((ds1307_rblock(0x12,tdata,7) & 0xc0) >> 6) * 10)/4;
    return temp*1000*templ*10;
}
#include "i2c_soft.h"


struct {
  unsigned char busy : 1;
} I2C_H;


void delay(unsigned int time){
  while(time--);
}

/*****************************************************************
 * @brief :  ham i2c thi gom 4 ham co ban
 * start,stop , write, read
 ******************************************************************/

void start_i2c(){
  SDA = 1;
  SCL = 1;
  delay(TIME_DELAY);
  SDA = 0;
  SCL = 0;
  delay(TIME_DELAY);
}

void stop_i2c(){
  SDA = 1;
  SCL = 1;
  delay(TIME_DELAY);
  SDA = 0;
  SDA = 1;
  delay(TIME_DELAY);
}

void write_byte_i2c(unsigned char byte){
  unsigned char i;
  for(i = 0; i < 8; i++){
    SDA = (byte & 0x80) ? 1 : 0;
    SCL = 1;
    delay(TIME_DELAY);
    SCL = 0;
    byte <<= 1;
    delay(TIME_DELAY);
  }
  SDA = 1;
  SCL = 1;
  delay(TIME_DELAY);
  SCL = 0;
  delay(TIME_DELAY);
}

unsigned char read_byte_i2c(unsigned char address){
  unsigned char i, result;
  start_i2c();
  write_byte_i2c(ds3231_slavew);
  write_byte_i2c(address);
  //stop_i2c();
  start_i2c();
  write_byte_i2c(ds3231_slaver);
  SDA_DDR = 0;
  for(i = 0; i < 8; i++){
    SCL = 1;
    delay(TIME_DELAY);
    result <<= 1;
    result |= SDA_IDR;
    SCL = 0;
    delay(TIME_DELAY);
  }
  SDA_DDR = 1;
  SDA = 1;
  SCL = 1;
  delay(TIME_DELAY);
  SCL = 0;
  delay(TIME_DELAY);
  stop_i2c();
  return result;
}

void write_bcd_i2c(unsigned char byte){
  write_byte_i2c(byte / 10 * 16 + byte % 10);
}

unsigned char read_bcd_i2c(unsigned char address){
  unsigned char res;
  res = read_byte_i2c(address);
  return res / 16 * 10 + res % 16;
}

void read_time(unsigned char *hours, unsigned char *minutes, unsigned char *seconds){
  while(I2C_H.busy);
  I2C_H.busy = 1;
  *seconds = read_bcd_i2c(0x00);
  *minutes = read_bcd_i2c(0x01);
  *hours = read_bcd_i2c(0x02);
  I2C_H.busy = 0;
}

void read_date(unsigned char *w, unsigned char *dd, unsigned char *mm, unsigned char *yy){
  while(I2C_H.busy);
  I2C_H.busy = 1;
  *w = read_bcd_i2c(0x03) % 7;
  *dd = read_bcd_i2c(0x04) % 32;
  *mm = read_bcd_i2c(0x05) % 13;
  *yy = read_bcd_i2c(0x06) % 100;
  I2C_H.busy = 0;
}

void save_time(unsigned char h, unsigned char m, unsigned char s){
  while(I2C_H.busy);
  I2C_H.busy = 1;
  start_i2c();
  write_byte_i2c(ds3231_slavew);
  write_byte_i2c(0x00);
  write_bcd_i2c(s);
  write_bcd_i2c(m);
  write_bcd_i2c(h);
  stop_i2c();
  I2C_H.busy = 0;
}

void save_date(unsigned char thu, unsigned char d, unsigned char m, unsigned char y){
  while(I2C_H.busy);
  I2C_H.busy = 1;
  start_i2c();
  write_byte_i2c(ds3231_slavew);
  write_byte_i2c(0x03);
  write_bcd_i2c(thu);
  write_bcd_i2c(d);
  write_bcd_i2c(m);
  write_bcd_i2c(y);
  stop_i2c();
  I2C_H.busy = 0;
}

void init_rtc(){
  I2C_H.busy = 0;
   
}


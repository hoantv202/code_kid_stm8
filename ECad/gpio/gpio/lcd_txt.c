#include "lcd_txt.h"

volatile uint32_t time_keeper=0;




void Delay_isr(void)
{
 
  if(TIM4_GetITStatus(TIM4_IT_UPDATE)==SET)
    {
      if(time_keeper!=0)
        {
          time_keeper--;
          }
      TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
      }

}
void Delay_ms(uint32_t time)
{
  time_keeper = time;
  while(time_keeper);
  }

/*--------------- Initialize LCD ------------------*/
void lcd_init(void)
{
	
	Delay_ms(50);
     
	
	GPIO_WriteLow(D4_PORT,D4_PIN);
	GPIO_WriteHigh(D5_PORT,D5_PIN);
	GPIO_WriteLow(D6_PORT,D6_PIN);
	GPIO_WriteLow(D7_PORT,D7_PIN);
	GPIO_WriteLow(RS_PORT,RS_PIN);
	
	GPIO_WriteHigh(EN_PORT,EN_PIN);
	GPIO_WriteLow(EN_PORT,EN_PIN);
	
	lcd_write(0,0x28);
	lcd_write(0,0x0c);
	lcd_write(0,0x06);
	lcd_write(0,0x01);
}

/*--------------- Write To LCD ---------------*/
void lcd_write(uint8_t type,uint8_t data)
{
	Delay_ms(5);
      
	if(type)
	{
		GPIO_WriteHigh(RS_PORT,RS_PIN);
	}else
	{
		GPIO_WriteLow(RS_PORT,RS_PIN);
	}
	
	//Send High Nibble
	if(data&0x80)
	{
		GPIO_WriteHigh(D7_PORT,D7_PIN);
	}else
	{
		GPIO_WriteLow(D7_PORT,D7_PIN);
	}
	
	if(data&0x40)
	{
		GPIO_WriteHigh(D6_PORT,D6_PIN);
	}else
	{
		GPIO_WriteLow(D6_PORT,D6_PIN);
	}
	
	if(data&0x20)
	{
		GPIO_WriteHigh(D5_PORT,D5_PIN);
	}else
	{
		GPIO_WriteLow(D5_PORT,D5_PIN);
	}
	
	if(data&0x10)
	{
		GPIO_WriteHigh(D4_PORT,D4_PIN);
	}else
	{
		GPIO_WriteLow(D4_PORT,D4_PIN);
	}
	GPIO_WriteHigh(EN_PORT,EN_PIN);
	GPIO_WriteLow(EN_PORT,EN_PIN);
	

	//Send Low Nibble
	if(data&0x08)
	{
		GPIO_WriteHigh(D7_PORT,D7_PIN);
	}else
	{
		GPIO_WriteLow(D7_PORT,D7_PIN);
	}
	
	if(data&0x04)
	{
		GPIO_WriteHigh(D6_PORT,D6_PIN);
	}else
	{
		GPIO_WriteLow(D6_PORT,D6_PIN);
	}
	
	if(data&0x02)
	{
		GPIO_WriteHigh(D5_PORT,D5_PIN);
	}else
	{
		GPIO_WriteLow(D5_PORT,D5_PIN);
	}
	
	if(data&0x01)
	{
		GPIO_WriteHigh(D4_PORT,D4_PIN);
	}else
	{
		GPIO_WriteLow(D4_PORT,D4_PIN);
	}
	GPIO_WriteHigh(EN_PORT,EN_PIN);
	GPIO_WriteLow(EN_PORT,EN_PIN);
}

void lcd_puts(uint8_t x, uint8_t y, int8_t *string)
{
	//Set Cursor Position
	#ifdef LCD16xN	//For LCD16x2 or LCD16x4
	switch(x)
	{
		case 0: //Row 0
			lcd_write(0,0x80+0x00+y);
			break;
		case 1: //Row 1
			lcd_write(0,0x80+0x40+y);
			break;
		case 2: //Row 2
			lcd_write(0,0x80+0x10+y);
			break;
		case 3: //Row 3
			lcd_write(0,0x80+0x50+y);
			break;
	}
	#endif
	
	#ifdef LCD20xN	//For LCD20x4
	switch(x)
	{
		case 0: //Row 0
			lcd_write(0,0x80+0x00+y);
			break;
		case 1: //Row 1
			lcd_write(0,0x80+0x40+y);
			break;
		case 2: //Row 2
			lcd_write(0,0x80+0x14+y);
			break;
		case 3: //Row 3
			lcd_write(0,0x80+0x54+y);
			break;
	}
	#endif
	
	while(*string)
	{
		lcd_write(1,*string);
		string++;
	}
}
void lcd_clear(void)
{
	lcd_write(0,0x01);
}




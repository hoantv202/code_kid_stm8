#ifndef		__LED7SEG__H
#define		__LED7SEG__H

#include "stm8s.h"


/* Private defines -----------------------------------------------------------*/
#define SCLK_PORT		GPIOC
#define SCLK_PIN		GPIO_PIN_5
#define SCLK_ON			GPIO_WriteHigh(SCLK_PORT, SCLK_PIN);
#define SCLK_OFF		GPIO_WriteLow(SCLK_PORT, SCLK_PIN);

#define SDAT_PORT		GPIOC
#define SDAT_PIN		GPIO_PIN_6
#define SDAT_ON			GPIO_WriteHigh(SDAT_PORT, SDAT_PIN);
#define SDAT_OFF		GPIO_WriteLow(SDAT_PORT, SDAT_PIN);

#define SLCH_PORT		GPIOE
#define SLCH_PIN		GPIO_PIN_5
#define SLCH_ON			GPIO_WriteHigh(SLCH_PORT, SLCH_PIN);
#define SLCH_OFF		GPIO_WriteLow(SLCH_PORT, SLCH_PIN);

/* Private function prototypes -----------------------------------------------*/
void sysClkConfig(void);
void Led7segConfig(void);
void shifterWrite(unsigned char sdata);
void E4094_Out(unsigned char *p, unsigned char n);
void shifterTransmit(unsigned char *arrData);
void shifterClock(void);
void shifterLatch(void);
void display(uint8_t pos, uint8_t num);
void Hienthiso(unsigned long data);
/* Private Variables ---------------------------------------------------------*/



#endif
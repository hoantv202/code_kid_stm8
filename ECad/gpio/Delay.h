#ifndef __DELAY__H
#define __DELAY__H

#include "stm8s.h"

void delay_using_timer4_init(void);
void Delay_isr(void);
void Delay_ms(uint32_t time);



#endif